﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTTPServer
{
    interface ISerializer
    {
        string Serialize<T>(T someData);
        T Deserialize<T>(string serializedString);
        object GetSerializer(Type key);
    }
}
