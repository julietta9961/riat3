﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;

namespace HTTPServer
{
    class Server
    {
        TcpListener Listener;
        Client client = new Client();
        public Server(int Port)
        {
            Listener = new TcpListener(IPAddress.Any, Port);
            Listener.Start();
            do
            {
               
                ClientThread(Listener.AcceptTcpClient());
                
            } while (client.powerOn);
        }

        void ClientThread(Object StateInfo)
        {
            client.Start((TcpClient)StateInfo);
            if (!client.powerOn) Listener.Stop();
        }

        ~Server()
        {
            if (Listener != null)
            {
                Listener.Stop();
            }
        }

        static void Main(string[] args)
        {
            new Server(Convert.ToInt32(Console.ReadLine()));
        }
    }
}
