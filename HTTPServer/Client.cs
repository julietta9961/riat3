﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;
using System.Reflection;
namespace HTTPServer
{
    class Client
    {
        public string RequestURI { get; private set; }
        public bool powerOn { get; set; }
        protected JsonSerialize jsonSerializer = new JsonSerialize();
        protected Input input { get; set; }
        protected Output output { get; set; }

        protected string ReadClientMessage(TcpClient Client)
        {
            byte[] Buffer = new byte[Client.ReceiveBufferSize];
            int count;
            try
            {
                count = Client.GetStream().Read(Buffer, 0, Buffer.Length);
                return (count > 0) ? Encoding.UTF8.GetString(Buffer, 0, count) : null;
            }
            catch
            {
                Client.Close();
                return null;
            }
        }
        public void Start(TcpClient Client)
        {
            string Request = ReadClientMessage(Client);
            if (!Client.Connected) return;
            Match ReqMatch = Regex.Match(Request, @"^\w+\s+[\W]+([^\s\?]+)[^\s]*\s+HTTP/.*|");
            if (ReqMatch == Match.Empty)//Если запрос не удался
            {
                SendError(Client, 400);
                return;
            }
            string RequestUri = ReqMatch.Groups[1].Value;
            RequestUri = Uri.UnescapeDataString(RequestUri);

            if (RequestUri.IndexOf("..") >= 0)
            {
                SendError(Client, 400);
                return;
            }
            Reflection(Client, Request, RequestUri);


        }
        private void Reflection(TcpClient client, string Request, string RequestUri)
        {
            MethodInfo[] methots = this.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance);
            for (int I = 0; I < methots.Length; I++)
            {
                if (methots[I].Name == RequestUri)
                {
                    if (methots[I].GetParameters().Length == 1)
                    {
                        methots[I].Invoke(this, new object[] { client });
                    }
                    else
                    {
                        methots[I].Invoke(this, new object[] { client, Request });
                    }
                    return;
                }

            }
            SendError(client, 400);
        }

        protected void SendError(TcpClient Client, int Code)
        {
            string Header = String.Format("HTTP/1.1 {0} {1}\n\n",Code,(HttpStatusCode)Code);
            byte[] buf = Encoding.UTF8.GetBytes(Header);
            Client.GetStream().Write(buf, 0, buf.Length);
            Client.Close();
        }

        public Client()
        {
            powerOn = true;
        }

        private void Ping(TcpClient Client)
        {
            string Header = "HTTP/1.1 200 OK\nContent-type: text/json\nContent-Length:0\n\n";
            byte[] buf = Encoding.UTF8.GetBytes(Header);
            Client.GetStream().Write(buf, 0, buf.Length);
            Client.Close();
        }

        private void GetAnswer(TcpClient Client)
        {
            output = InputToOutput.MakeOutputFromInput(input);
            WriteResponse<Output>(output, Client);

        }

        private void PostInputData(TcpClient Client, string Request)
        {
            try {
                if (Request.IndexOf("Expect: 100-continue") > 0)
                {
                     Expect100Handler(Client);
                    if (!Client.Connected) return;
                }
                input = GetBody<Input>(Client);
                if(input==null)
                {
                    SendError(Client, 400);
                }
                string Header = "HTTP/1.1 200 OK\nContent-type: text/json\nContent-Length:0\n\n";
                byte[] buf = Encoding.UTF8.GetBytes(Header);
                Client.GetStream().Write(buf, 0, buf.Length);
                Client.Close();

            }
            catch
            {
                SendError(Client, 400);
            }
        }


        private void Stop(TcpClient Client)
        {

            string Header = "HTTP/1.1 200 OK\nContent-type: text/json\nContent-Length:0\n\n";
            byte[] buf = Encoding.UTF8.GetBytes(Header);
            Client.GetStream().Write(buf, 0, buf.Length);
            Client.Close();            
            powerOn = false;

        }
        private string Expect100Handler(TcpClient Client)
        {
            string Header = String.Format("HTTP/1.1 {0} {1}\n\n", 100, (HttpStatusCode)100);
            byte[] buf = Encoding.UTF8.GetBytes(Header);
            Client.GetStream().Write(buf, 0, buf.Length);
            return null;
        }



        private void WriteResponse <T>(T data ,TcpClient Client )
        {
           
            string jsonObj = jsonSerializer.Serialize<T>(data);
            string Header = String.Format("HTTP/1.1 200 OK\nContent-type: text/json\nContent-Length:{0}\n\n{1}",
                                            jsonObj.Length,
                                            jsonObj);
            byte[] buf = Encoding.UTF8.GetBytes(Header);
            Client.GetStream().Write(buf, 0, buf.Length);
            Client.Close();
        }

        private T GetBody <T>(TcpClient Client)
        {
          string Message = ReadClientMessage(Client);
             if (!Client.Connected)
            {
                object o=null;
                return (T)o;
            }
            return jsonSerializer.Deserialize<T>(Message);
            
        }

    }
}
