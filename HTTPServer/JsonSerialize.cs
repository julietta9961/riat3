﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HTTPServer
{
    class JsonSerialize 
    {
        public T Deserialize<T>(string serializedData)
        {
           
            return (T)JsonConvert.DeserializeObject<T>(serializedData);
        }

        public string Serialize<T>(T data)
        {
            return JsonConvert.SerializeObject(data);
        }
    }
}
