﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTTPServer
{
    public class InputToOutput
    {
        public static Output MakeOutputFromInput(Input input)
        {
            var output = new Output();
            output.SumResult = input.Sums.Select(i => i * input.K).Sum();
            output.MulResult = input.Muls.Aggregate((acc, i) => acc * i);
            output.SortedInputs = input.Sums.Concat(input.Muls.Select(i => (decimal)i))
                                            .OrderBy(x => x)
                                            .ToArray();
            return output;
        }
    }
}
